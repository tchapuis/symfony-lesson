<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200930083022 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE job (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_person (job_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_64D8FCF7BE04EA9 (job_id), INDEX IDX_64D8FCF7217BBB47 (person_id), PRIMARY KEY(job_id, person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(45) NOT NULL, email VARCHAR(255) DEFAULT NULL, age VARCHAR(10) NOT NULL, prenom VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE job_person ADD CONSTRAINT FK_64D8FCF7BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE job_person ADD CONSTRAINT FK_64D8FCF7217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job_person DROP FOREIGN KEY FK_64D8FCF7BE04EA9');
        $this->addSql('ALTER TABLE job_person DROP FOREIGN KEY FK_64D8FCF7217BBB47');
        $this->addSql('DROP TABLE job');
        $this->addSql('DROP TABLE job_person');
        $this->addSql('DROP TABLE person');
    }
}
