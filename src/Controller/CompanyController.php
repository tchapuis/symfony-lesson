<?php

namespace App\Controller;

use App\Entity\Company;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CompanyController extends AbstractController
{
    /**
     * @Route("/company/new", name="new_company")
     */
    public function newCompany(EntityManagerInterface $entityManager)
    {
        $company = new Company();
        $company->setName('Chapuis Corp');
        $company->setAdress('Simplon');
        $company->setSiret('123456789');

        $company2 = new Company();
        $company2->setName('Chapuis Corp');
        $company2->setAdress('Simplon');
        $company2->setSiret('123456789');

        $entityManager->persist($company);
        $entityManager->persist($company2);
        $entityManager->flush();

        return $this->render('company/index.html.twig');
    }

    /**
     * @Route("/company/list", name="listCompany")
     */
    public function listCompany(EntityManagerInterface $entityManager)
    {
        $companyRepository = $entityManager->getRepository(Company::class);

        //$arrayCompanies = $companyRepository->findAll();
        $arrayCompanies = $companyRepository->findBy([
            'name' => 'Chapuis Corp',
            'siret' => '123456789'
        ]);

        //dump($arrayCompanies);exit();

        return $this->render('company/list.html.twig', [
            'companies' => $arrayCompanies
        ]);
    }

    /**
     * @Route("/company/{id}", name="showCompany")
     */
    public function showCompany(EntityManagerInterface $entityManager, $id)
    {
        $companyRepository = $entityManager->getRepository(Company::class);

        $company = $companyRepository->find($id);

        return $this->render('company/show.html.twig', [
            'company' => $company
        ]);
    }

    /**
     * @Route("/company/edit/{id}", name="editCompany")
     */
    public function editCompany(EntityManagerInterface $entityManager, $id)
    {
        $companyRepository = $entityManager->getRepository(Company::class);

        $company = $companyRepository->find($id); // 3

        $company->setName('Pixeliz');
        $company->setSiret('987654321');

        $entityManager->flush();

        return $this->render('company/show.html.twig', [
            'company' => $company
        ]);
    }

    /**
     * @Route("/company/delete/{id}", name="deleteCompany")
     */
    public function deleteCompany(EntityManagerInterface $entityManager, $id)
    {
        $companyRepository = $entityManager->getRepository(Company::class);

        $company = $companyRepository->find($id);

        $entityManager->remove($company);
        $entityManager->flush();

        return $this->render('company/show.html.twig', [
            'company' => $company
        ]);
    }
}
