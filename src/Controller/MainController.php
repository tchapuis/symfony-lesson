<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/main", name="main")
     */
    public function index()
    {
        $array = [
          'Banane',
          'Pomme',
          'Poire'
        ];
        return $this->render('main/index.html.twig', [
            'foods' => $array,
        ]);
    }
}
