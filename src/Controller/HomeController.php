<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home", name="home")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request)
    {
        dump($request->query->all());
    }

    /**
     * @Route("/{name}", name="getContact")
     */
    public function getContact($name)
    {
        return $this->render('home/contact.html.twig', [
            'name' => $name
        ]);
    }

    /**
     * @Route("/contact", name="contact", priority="2")
     */
    public function contact()
    {
        // Du JSON
        return $this->json(['name' => 'Thomas']);
        // Du Fichier
        return $this->file('/pdf/truc.pdf');
    }

    /**
     * @Route("/agents/suppr/{code}",
     *     name="deleteAgent",
     *     requirements={"code"="\d+"},
     *     methods={"GET"}
     *     )
     */
    public function deleteAgent($code)
    {
        echo $code;
        return $this->render('home/contact.html.twig');
    }

    /**
     * @Route("/plus/{a}/{b}",
     *     name="somme",
     *     requirements={"a"="\d+", "b"="\d+"},
     *     methods={"GET"}
     *     )
     */
    public function somme(int $a, int $b)
    {
        $somme = $a + $b;
        return $this->json([
           'somme' => $somme
        ]);
    }
}
